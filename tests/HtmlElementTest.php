<?php

namespace Tests;
use App\HtmlElement;

class HtmlElementTest extends TestCase
{
    /** @test */
    public function it_generates_html_elements()
    {
        $element = new HtmlElement('p', [], 'Este es el contenido');

        $this->assertSame(
            '<p>Este es el contenido</p>',
            $element->render()
        );
    }
}
